Тестовое задание от Bonstongene

# Yandex translate API #

У Яндекс переводчика (translate.yandex.ru) имеется REST API. Необходимо реализовать консольное приложение, которое переводит введенную английскую фразу на русский язык, пользуясь предложенным API


### Сборка ###

> mvn clean compile assembly:single