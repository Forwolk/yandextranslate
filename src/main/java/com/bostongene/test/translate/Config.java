package com.bostongene.test.translate;

public interface Config {

    String TOKEN = "trnsl.1.1.20170304T105932Z.4a5c9e56819a97ce.3a188856fc4f55450403b5ff1649ecfbd6e5bc89"; //Ключ API.
    String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate" +
            "?key=%key_api%" +
            "&text=%text%" +
            "&lang=%language%";
}
