package com.bostongene.test.translate;

import java.util.Scanner;

public class Main {

    public static void main (String... args) {
        Scanner in = new Scanner(System.in);

        while (true) {
            String string = in.nextLine();
            translateByYandex(string);
        }
    }

    private static void translateByYandex (String english) {
        Translator translator = YandexTranslator.getInstance();

        System.out.println(String.format("Перевод: %s", translator.translateEN_RU(english)));
        System.out.println("Переведено сервисом «Яндекс.Переводчик»");
    }
}
