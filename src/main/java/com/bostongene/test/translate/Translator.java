package com.bostongene.test.translate;

public interface Translator {

    /**
     * Перевод с английского на русский
     *
     * @param english Текст на английском
     * @return Текст на русском
     */
    String translateEN_RU (String english);
}
