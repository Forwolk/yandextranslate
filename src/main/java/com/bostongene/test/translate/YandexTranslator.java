package com.bostongene.test.translate;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;

public class YandexTranslator implements Translator {

    private static YandexTranslator instance;

    public static YandexTranslator getInstance () {
        if (instance == null)
            instance = new YandexTranslator();
        return instance;
    }

    private YandexTranslator () {}

    /**
     * Перевод с английского на русский
     *
     * @param english Текст на английском
     * @return Текст на русском
     */
    public String translateEN_RU(String english) {
        String russian = "ERROR: I couldn't translate this.";

        String prepareURL = Config.URL
                .replace("%key_api%", Config.TOKEN)
                .replace("%language%", "en-ru")
                .replace("%text%", english);

        try {
            String result = Util.getRequest(prepareURL);
            JsonParser parser = new JsonParser();
            JsonObject jsonObject = parser.parse(result).getAsJsonObject();
            russian = jsonObject.get("text").getAsString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return russian;
    }
}
